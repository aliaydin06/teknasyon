# teknasyon challenge

Merhabalar, eksik kısımlar olsada yapabildiğim kadarıyla projeyi paylaşmak istedim.

Şu an için $docker-compose up ile containerlar çalışıyor görünüyor, fakat veritabanı ile ilgili sorunlar yaşadım ve çözmek için maalesef vakit bulamadım. Yazdığım kısımları local makinamdaki Lamp ile çalıştırmak zorunda kaldım ve geriye dönecek vaktim maalesef kalmadı. 

Yinede değerlendirmenize sunmak için docker-compose.yml dosyasını paylaşıyorum.

Genel olarak yaptıklarımdan bahsedecek olursam;

database/migrations altında örnek uygulama için migration ve seed dosyaları mevcut. proje altında "php artisam migrate --seed" ile veritabanı ve örnek kayıtlar oluşturulabilir.

Api endpointlerine ulaşabilmek için önce "http://localhost/teknasyon/public/api/login" 'e' POST metoduyla email="test@test.com" ve password="secret" gönderilerek geriye "api_token" bilgisi döndürülebilir. Daha sonraki işlemlerde "api_token" headerda "Authorization Bearer {token}" şeklinde gönderilmelidir.

Yazılan metodlar :

Şarkı kategorilerini almak için:
GET http://localhost/teknasyon/public/api/categories

Bir kategorideki şarkıları almak için:
GET http://localhost/teknasyon/public/api/songs/{category_id}

Favorilere Şarkı Eklemek için:
POST http://localhost/teknasyon/public/api/favorites
parametre olarak id={song_id}

Favorilerden Şarkı Silmek için:
DELETE http://localhost/teknasyon/public/api/favorites/{id}
*id: Favori id'si

Favorileri Almak için:
GET http://localhost/teknasyon/public/api/favorites
(geçerli user_id sine göre filtreleniyor)

Logout için:
http://localhost/teknasyon/public/api/login

Değerlendirmelerinize sunarım

Ali Aydın