<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds = [
            ['id'=>1, 'name'=>'Kuş Sesleri'],
            ['id'=>2, 'name'=>'Doğa Sesleri'],
            ['id'=>3, 'name'=>'Piyano Sesleri'],
        ];

        foreach ($seeds as $seed) {
            DB::table('categories')->insert($seed);
        }

    }
}
