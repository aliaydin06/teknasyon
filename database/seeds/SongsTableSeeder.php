<?php

use Illuminate\Database\Seeder;

class SongsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds = [
            ['category_id'=>1, 'name'=>'Martı Sesi', 'file' => 's4ge23fr79.mp3'],
            ['category_id'=>1, 'name'=>'Bülbül Sesi', 'file' => 'cv56ha234h.mp3'],
            ['category_id'=>1, 'name'=>'Güvercin Sesi', 'file' => '6bde8jufs4.mp3'],
            ['category_id'=>2, 'name'=>'Deniz Sesi', 'file' => '4asv6hna3g.mp3'],
            ['category_id'=>2, 'name'=>'Yağmur Sesi', 'file' => '55dv68da3e.mp3'],
            ['category_id'=>2, 'name'=>'Orman Sesi', 'file' => '7hd4h51dcf.mp3'],
            ['category_id'=>3, 'name'=>'Vivaldi For Season', 'file' => 'gt678as4fd.mp3'],
            ['category_id'=>3, 'name'=>'Bethoven Moonlight Sonata', 'file' => 'nh71scfgh8.mp3'],
            ['category_id'=>3, 'name'=>'Pachelbel Canon In D', 'file' => 'dfr4d2dg0h.mp3'],
        ];

        foreach ($seeds as $seed) {
            DB::table('songs')->insert($seed);
        }
    }
}
