<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth:api'], function () {

    Route::get('me', 'Api\ApiController@getUser');

    Route::get('categories', 'Api\ApiController@getCategories');
    Route::get('songs/{category_id}', 'Api\ApiController@getCategorySongs');

    Route::get('favorites', 'Api\ApiController@getFavorites');
    Route::post('favorites', 'Api\ApiController@addFavorite');
    Route::delete('favorites/{id}', 'Api\ApiController@deleteFavorite');


});