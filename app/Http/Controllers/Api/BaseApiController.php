<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseApiController extends Controller
{

    public function success(array $data) {

        return response()->json([
            'errorCode' => '',
            'errorMessage' => '',
            'data' => $data,
        ], 200);

    }

    public function notfound() {

        return response()->json([
            'errorCode' => 'notFound',
            'errorMessage' => 'Resource not found!',
            'data' => [],
        ], 404);

    }

}
