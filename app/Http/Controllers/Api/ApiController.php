<?php

namespace App\Http\Controllers\Api;

use App\Models\Categorie;
use App\Models\Favorite;
use App\Models\Song;
use Illuminate\Http\Request;

class ApiController extends BaseApiController
{


    public function getCategories() {

        $data = Categorie::all();

        return $this->success($data->toArray());
    }

    public function getCategorySongs($category_id) {

        $data = Song::where('category_id', $category_id)->get();

        return $this->success($data->toArray());
    }

    public function getFavorites() {

        $user = auth()->user();

        $data = Favorite::where('user_id', $user->id)->get();

        return $this->success($data->toArray());
    }

    public function addFavorite(Request $request) {

        $user = auth()->user();

        $fav = new Favorite();
        $fav->user_id = $user->id;
        $fav->song_id = $request->song_id;
        $fav->save();

        return $this->success($fav->toArray());

    }

    public function deleteFavorite($id) {

        $fav = Favorite::find($id);

        if ($fav) {
            $fav->delete();
            return $this->success([]);
        }

        return $this->notfound();
    }


    public function getUser() {

        return $this->success(auth()->user()->toArray());

    }

}
